// Plays sound and highlights display keys when corresponding key is pressed
const playSound = function (e) {
  let audio = document.querySelector(`audio[data-key="${e.keyCode}"]`);
  let key = document.querySelector(`.key[data-key="${e.keyCode}"]`);

  if (!audio) return;

  // When an audio is playing, it won't play until it is finished. During this time, if the key is repeatedly pressed, no sound will play. Thus we need to rewind the audio to initial state everytime a key is pressed
  audio.currentTime = 0;
  audio.play();

  // Adding visual effect
  key.classList.add('playing');
};

const removeStyling = function (e) {
  if (e.propertyName === 'transform') {
    this.classList.remove('playing');
  }
};

let keys = document.querySelectorAll('.key');
keys.forEach((key) => {
  // When transition completes, remove the highlighting visual effect
  key.ontransitionend = removeStyling;
});

document.onkeydown = playSound;
