let secondHand = document.querySelector('.hand--second');
let minuteHand = document.querySelector('.hand--minute');
let hourHand = document.querySelector('.hand--hour');

const adjustClock = () => {
  // get the current time
  let currentTime = new Date(Date.now());

  // rotating the second hand
  let seconds = currentTime.getSeconds();
  let secHandDegree = (seconds * 360) / 60;
  secondHand.style.transform = `rotate(${secHandDegree}deg)`;

  // rotating the minute hand
  let minutes = currentTime.getMinutes();
  let minHandDegree = (minutes * 360) / 60;
  minuteHand.style.transform = `rotate(${minHandDegree}deg)`;

  // rotating the hour hand
  let hours = currentTime.getHours();
  let hourHandDegree = (hours * 360) / 12;
  hourHand.style.transform = `rotate(${hourHandDegree}deg)`;
};

// adjustClock will run after every 1s
setInterval(adjustClock, 1000);
